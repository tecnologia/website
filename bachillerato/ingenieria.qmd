---
title: "Tecnología e Ingeniería I y II"
image: ingenieria.png
listing:
  contents:
    - "../bloques/Ingenieria/*.qmd"
---

<script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>

---

<iconify-icon icon="carbon:time" width="24"></iconify-icon> 4 horas semanales

<iconify-icon icon="material-symbols:lab-research-outline-sharp" width="24">></iconify-icon> Taller de tecnología

<iconify-icon icon="icon-park-outline:document-folder" width="24">></iconify-icon> [Currículo](https://www.gobiernodecanarias.org/cmsweb/export/sites/educacion/web/_galerias/descargas/bachillerato/curriculo/nuevo_curriculo/julio_2022/Tecnologia_e_Ingenieria_BACH.pdf)

<span style="font-size:28px">**Bloques**</span>
---

