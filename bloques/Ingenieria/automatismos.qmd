---
title: "Sistemas automáticos"
image: automatismos.png
---
<script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>
---
<iconify-icon icon="gis:map-control" width="24"></iconify-icon> Análisis de elementos de sistemas de control

<iconify-icon icon="la:project-diagram" width="24"></iconify-icon> Álgebra de bloques

<iconify-icon icon="ant-design:control-outlined" width="24"></iconify-icon> Condiciones de estabilidad de sistemas automáticos

<iconify-icon icon="tabler:devices-pc" width="24"></iconify-icon> Simuladores
