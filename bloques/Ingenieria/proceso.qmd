---
title: "Proyectos de investigación y desarrollo"
image: proyectos.png
---
<script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>
---
<iconify-icon icon="teenyicons:bulb-on-outline" width="24"></iconify-icon> Ideas y diseño de proyectos

<iconify-icon icon="mdi:head-thinking-outline" width="24"></iconify-icon> Resolución de problemas

<iconify-icon icon="ri:presentation-line" width="24"></iconify-icon> Publicación y difusión del proyecto
