---
title: "Digitalización del entorno personal de aprendizaje"
image: digitalizacion.png
---

<script src="https://code.iconify.design/iconify-icon/1.0.7/iconify-icon.min.js"></script>
---
<iconify-icon icon="ic:baseline-search" width="24"></iconify-icon> Búsqueda y selección de información

<iconify-icon icon="streamline:interface-edit-write-2-change-document-edit-modify-paper-pencil-write-writing" width="18"></iconify-icon> Creación y reutilización de contenidos

<iconify-icon icon="vscode-icons:file-type-publisher" width="24"></iconify-icon> Colaboración y difusión de contenidos

<iconify-icon icon="mdi:head-thinking-outline" width="24"></iconify-icon> Fomentar actitud crítica en la búsqueda de
información

<iconify-icon icon="simple-icons:creativecommons" width="24"></iconify-icon> Respeto de los derechos de autor y la propiedad intelectual


